package org.netlify.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.netlify.body.Footer;
import org.netlify.body.Header;

import static com.codeborne.selenide.Selenide.$;

public class HomePage extends Page{
    private final String title = Selenide.title();

    private  Header header;
    private final Footer footer;
    private final SortPage sortPage;
    private final SearchPage searchPage;
    private final SelenideElement modal = $(".modal-dialog");
    private final SelenideElement pageWishlistSubtitle = $(".text-muted");
    private final SelenideElement pageCartSubtitle = $("small.text-muted");
    private final SelenideElement sortField = $(".sort-products-select");
    private  final SelenideElement searchField = $("#input-search");
    private  final SelenideElement searchButton = $(".btn.btn-light.btn-sm");
    public HomePage() {
        System.out.println("Constructing Header.");
        this.header = new Header();
        System.out.println("Constructing Footer.");
        this.footer = new Footer();
        this.sortPage = new SortPage();
        this.searchPage = new SearchPage();
    }
    /**
     * Getters
     */
    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getPageTitle() {
        return title;
    }
    /**
     * Validators
     */
    public void validateThatHeaderContainsLogoIcon() {
        System.out.println("               ----------");
        System.out.println("Verify that logo is : "+ header.getLogoIcon());
    }

    public void validateThatHeaderContainsShoppingCartIcon() {
        System.out.println("Verify that shopping cart is : "+ header.getCartIcon());
    }

    public void validateThatHeaderContainsWishlistIcon() {
        System.out.println("Verify that wishlist is : "+ header.getWishListIcon());
    }

    public void validateThatHeaderContainsGreetingsMessage() {
        System.out.println("Verify that greetings message is : "+ header.getGreetingsMessage());

    }

    public void validateThatHeaderContainsLoginButton() {
        System.out.println("Verify that login button is : "+ header.getOpenModalButton());
        System.out.println("               ----------");
    }

    public void validateThatFooterContainsDetails() {
        System.out.println("Verify that details is : " +footer.getDetails());
    }

    public void validateThatFooterContainsQuestionIcon() {
        System.out.println("Verify that question icon is : "+ footer.getQuestionIcon());
    }

    public void validateThatFooterContainsResetIcon() {
        System.out.println("Verify that reset icon is : "+ footer.getResetIcon());
    }


    public boolean validateModalIsDisplayed() {
        System.out.println("Verify that the modal is displayed on page. ");
        return this.modal.exists() && this.modal.isDisplayed();
    }
    public boolean validateModalIsNotDisplayed() {
        System.out.println("Verify that modal is not on page. ");
        return modal.exists();

    }
    public boolean validateWishlistIsNotDisplayed() {
        return pageWishlistSubtitle.exists();
    }
    public boolean validateCartIsNotDisplayed() {
        return pageCartSubtitle.exists();
    }
    public boolean validateSortFieldIsDisplayed() {
        return this.sortField.exists() && this.sortField.isDisplayed();
    }

    public boolean validateSearchFieldIsDisplayed() {
        System.out.println("Verify that search field is displayed.");
        return this.searchField.exists() && this.searchField.isDisplayed();
    }

    public boolean validateSearchButtonIsDisplayed() {
        System.out.println("Verify that search button is displayed.");
        return this.searchButton.exists() && this.searchButton.isDisplayed();
    }

    public boolean validateSearchButtonIsEnabled() {
        System.out.println("Verify that search button is enabled. ");
        return this.searchButton.isEnabled();
    }




    /**
     * Clicks
     */
    public void clickOnTheOpenModalButton() {
        System.out.println("Clicked on the open Modal Button.");
        this.header.clickOnTheOpenModalButton();
    }
    public void clickOnTheWishlistIcon() {
        this.header.clickOnTheWishlistIcon();
    }
    public void clickOnTheLogoButton() {
        this.header.clickOnTheLogoButton();
    }

    public void clickOnTheCartIcon() {
        this.header.clickOnTheCartIcon();
    }

    public void clickOnTheSortField() {
        this.sortPage.clickOnTheSortField();

    }



}
