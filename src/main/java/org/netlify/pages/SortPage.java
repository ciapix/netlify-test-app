package org.netlify.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SortPage {

    private final SelenideElement sortField = $(".sort-products-select");
    private final SelenideElement sortAToZ = $("option:first-child");
    private final SelenideElement sortZToA = $("option:nth-of-type(2)");
    private final SelenideElement sortLowToHigh = $("option:nth-of-type(3)");
    private final SelenideElement sortHighToLow = $("option:last-child");


    public void clickOnTheSortField() {
        this.sortField.click();

    }

    public boolean validateSortAToZIsDisplayedByDefault() {
        System.out.println("Verify that Sort by name (A to Z) is displayed.");
        return this.sortAToZ.exists() && this.sortAToZ.isDisplayed();
    }

    public boolean validateSortZToAIsDisplayed() {
        System.out.println("Verify that Sort by name (Z to A) is displayed.");
        return this.sortZToA.exists() && this.sortZToA.isDisplayed();
    }

    public boolean validateSortByPriceLowToHighIsDisplayed() {
        System.out.println("Verify that Sort by price (low to high) is displayed.");
        return this.sortLowToHigh.exists() && this.sortLowToHigh.isDisplayed();
    }

    public boolean validateSortByPriceHighToLowIsDisplayed() {
        System.out.println("Verify that Sort by price (high to low) is displayed.");
        return this.sortHighToLow.exists() && this.sortHighToLow.isDisplayed();
    }
}
