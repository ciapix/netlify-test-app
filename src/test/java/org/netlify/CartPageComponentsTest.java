package org.netlify;

import org.netlify.pages.CartPage;
import org.netlify.pages.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class CartPageComponentsTest {
    HomePage homePage = new HomePage();

    @Test
    public void cartPageComponents() {
        homePage.clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        assertTrue(cartPage.validateCartIsDisplayed(), "Expected cart page to be displayed.");
        assertTrue(cartPage.validateCartAddingMsgIsDisplayed(), "Expected cart adding message to be displayed.");
        homePage.clickOnTheLogoButton();
        assertTrue(homePage.validateCartIsNotDisplayed(), "Expected cart page to be closed.");
    }

}
