package org.netlify;

import org.netlify.pages.HomePage;
import org.netlify.pages.SortPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

public class SortPageComponentsTest {
    HomePage homePage = new HomePage();

    @Test

    public void verifySortPageComponents() {
        assertTrue(homePage.validateSortFieldIsDisplayed(), "Expected sort field to be displayed.");
        SortPage sortPage = new SortPage();
        assertTrue(sortPage.validateSortAToZIsDisplayedByDefault(), "Expected Sort by name (A to Z) to be displayed.");
        sleep(3000);
        homePage.clickOnTheSortField();
        sleep(3000);
        assertTrue(sortPage.validateSortZToAIsDisplayed(), "Expected Sort by name (Z to A) to be displayed.");
        assertTrue(sortPage.validateSortByPriceLowToHighIsDisplayed(), "Expected Sort by price (Low to High) to be displayed.");
        assertTrue(sortPage.validateSortByPriceHighToLowIsDisplayed(),"Expected Sort by price (High to Low) to be displayed.");
        homePage.clickOnTheSortField();
        sleep(3000);
    }


}
